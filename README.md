# imagemagick

Ansible role that installs and configures ImageMagick.


You can easily check the current resource limits with:
```
$ convert -list resource
Resource limits:
 Width: 128KP
 Height: 128KP
 Area: 1.0737GP
 Memory: 2GiB
 Map: 4GiB
 Disk: 8GiB
 File: 768
 Thread: 8
 Throttle: 0
 Time: unlimited
```


## Why are resource limits reset by themselves?

Some time has passed since the last time I ran this role,
but what has caused the resource limits to reset to their
poor defaults?

Does that happen when ImageMagick is updated, perhaps?

Anyway, re-running this role immediately changes the resource
limits (no need to restart any service).

